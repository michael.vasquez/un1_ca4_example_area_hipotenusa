#  calcular el area de un retangulo
# autor =   "Michael Vasquzez"
# email =   "michael.vasquez@unl.edu.ec"

def area_retangulo (b,h):
    area = b * h
    return area
try:
    base = float(input("Ingrese la base:\t"))
    altura = float(input("Ingrese la altura:\t"))
    print("El area del retangulo es: ", area_retangulo(base, altura))
except:
    print("ERROR\nIngrese numeros")