#  calcular el area de un circulo
# autor =   "Michael Vasquzez"
# email =   "michael.vasquez@unl.edu.ec"

import math

def area_circulo (r):
    R = (r**2) * math.pi
    return R
try:
    radio = float(input("Ingrese el radio:\t"))
    print("El area del circulo es: ", area_circulo(radio))
except:
    print("ERROR\nIngrese numeros")