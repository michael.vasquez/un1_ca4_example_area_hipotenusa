#  calcular la hipotenusa al saber el valor de los catetos
# autor =   "Michael Vasquzez"
# email =   "michael.vasquez@unl.edu.ec"
import math

def hipotensa_valor (ca1,ca2):
    h = math.sqrt((ca1**2)+(ca2**2))
    return h
try:
    cateto1 = float(input("Ingrese el primer cateto:\t"))
    cateto2 = float(input("Ingrese el segundo cateto:\t"))
    print("El valor de la hipotenusa es: ", hipotensa_valor(cateto1, cateto2))
except:
    print("ERROR\nIngrese numeros")